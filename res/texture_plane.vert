#version 300 es
precision highp float;

out vec2 tex_coords;

in vec3 pos;
in vec2 in_tex_coords;

void main() {
    tex_coords = in_tex_coords;
    gl_Position = vec4(pos, 1.0);
}
