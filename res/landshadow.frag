#version 300 es
precision highp float;
out vec4 frag_color;

const int NUM_CASCADES = 4;

in float haze;
in vec2 tex_coord;
in vec3 frag_pos;
in vec3 normal;
in vec4 frag_pos_light_space[NUM_CASCADES];
in float z;

uniform sampler2D land_texture;
uniform sampler2D cascade_texture;
uniform float cascade_ranges[NUM_CASCADES+1];
uniform vec3 light_direction;
uniform vec3 view_pos;

vec3 light_color = vec3(1.0);
float ambient_strength = 0.4;
float diffuse_strength = 0.6;
float specular_strength = 0.2;
vec3 mist_color = vec3(0.4, 0.9, 1.0);

void main() {
    /* frag_color = mix(texture(land_texture, tex_coord), mist_color, haze); */
    vec3 color = texture(land_texture, tex_coord).rgb;
    // ambient
    vec3 ambient = ambient_strength * light_color;
    // diffuse
    vec3 norm = normalize(normal);
    vec3 light_direction_normalized = normalize(-light_direction);
    float diff = max(dot(norm, light_direction_normalized), 0.0);
    vec3 diffuse = diff * light_color;
    // specular
    vec3 view_direction = normalize(view_pos - frag_pos);
    vec3 reflect_direction = reflect(light_direction_normalized, norm);
    float spec = pow(max(dot(view_direction, reflect_direction), 0.0), 32.0);
    vec3 specular = specular_strength * spec * light_color;
    // shadow
    int cascade = -1;
    for (int i=0; i<NUM_CASCADES; i++) {
        if (z < cascade_ranges[i+1]) {
            cascade = i;
            break;
        }
    }

    // if (cascade == -1) {
    //     color = vec3(0.0, 1.0, 1.0);
    // } else if (cascade == 0) {
    //     color = vec3(1.0, 0.0, 0.0);
    // } else if (cascade == 1) {
    //     color = vec3(0.0, 1.0, 0.0);
    // } else if (cascade == 2) {
    //     color = vec3(0.0, 0.0, 1.0);
    // } else if (cascade == 3) {
    //     color = vec3(1.0, 1.0, 1.0);
    // }

    float shadow = 0.0;
    if (cascade >= 0) {
        vec3 projection_coords = frag_pos_light_space[cascade].xyz / frag_pos_light_space[cascade].w;
        projection_coords = projection_coords * 0.5 + 0.5;
        vec2 shadow_uv = projection_coords.xy * 0.5;
        if (cascade % 2 == 1) shadow_uv.x += 0.5;
        if (cascade >= 2) shadow_uv.y += 0.5;
        float closest = texture(cascade_texture, shadow_uv).r;
        float bias = pow(float(cascade + 1), 1.1) * max(0.0001 * (1.0 - dot(norm, light_direction_normalized)), 0.00001);

        // float shadow = projection_coords.z - bias > closest ? 1.0 : 0.0;

        vec2 texel_size = 2.0 / vec2(textureSize(cascade_texture, 0));
        for (int x=-1; x<=1; x++) {
            for (int y=-1; y<=1; y++) {
                float d = texture(cascade_texture, shadow_uv + vec2(x, y)*texel_size).r;
                shadow += projection_coords.z - bias > d ? 1.0 : 0.0;
            }
        }
        shadow /= 9.0;
    } else {
        shadow = 0.5;
    }
    
    // vec3 result = (diffuse + ambient + specular) * color; // No shadow
    vec3 result = (ambient + (1.0 - shadow)*(diffuse + specular)) * color;

    frag_color = vec4(mix(result, mist_color, haze), 1.0);
}
