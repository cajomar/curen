#version 300 es

precision highp float;

out float haze;
out vec2 tex_coord;
out vec3 lighting;

in vec3 pos;
in vec2 tex_coord_in;
in vec3 normal;

uniform mat4 player_space;
uniform mat4 model;

uniform vec3 light_direction;
uniform vec3 view_pos;

void main() {
    vec3 vert_pos = (model * vec4(pos, 1.0)).xyz;
    gl_Position = player_space * vec4(vert_pos, 1.0);
    haze = clamp(pow(gl_Position.z/100.0, 2.0), 0.0, 1.0);
    tex_coord = tex_coord_in;

    // diffuse
    // vec3 norm = normalize(transpose(inverse(model)) * vec4(normal, 0.0)).xyz;
    vec3 norm = normalize((model * vec4(normal, 0.0)).xyz);
    float diffuse = clamp(dot(norm, -light_direction), 0.0, 1.0);
    // specular
    vec3 view_direction = normalize(view_pos - vert_pos);
    vec3 reflect_direction = normalize(reflect(light_direction, norm));
    float specular = pow(clamp(dot(view_direction, reflect_direction), 0.0, 1.0), 32.0);

    lighting = 0.4 * vec3(0.8, 0.9, 1.0) + diffuse*0.6*vec3(0.9, 1.0, 1.0) + specular * 0.3 * vec3(1.0);
}
