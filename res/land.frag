#version 300 es
precision highp float;

out vec4 frag_color;

in float haze;
in vec2 tex_coord;
in vec3 lighting;

uniform sampler2D land_texture;

vec3 mist_color = vec3(0.4, 0.9, 1.0);

void main() {
    vec3 color = texture(land_texture, tex_coord).rgb;
    vec3 result = color * lighting;
    frag_color = vec4(mix(result, mist_color, haze), 1.0);
}
