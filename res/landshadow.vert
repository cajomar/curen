#version 300 es

precision highp float;

const int NUM_CASCADES = 4;

out float haze;
out vec2 tex_coord;
out vec3 frag_pos;
out vec3 normal;
out vec4 frag_pos_light_space[NUM_CASCADES];
out float z;

in vec3 pos;
in vec2 tex_coord_in;
in vec3 norm;

uniform float cascade_ranges[NUM_CASCADES+1];
uniform mat4 light_spaces[NUM_CASCADES];
uniform mat4 player_space;
uniform mat4 model;

void main() {
    frag_pos = (model * vec4(pos, 1.0)).xyz;
    gl_Position = player_space * vec4(frag_pos, 1.0);
    haze = clamp(pow(gl_Position.z/100.0, 1.9), 0.0, 1.0);
    tex_coord = tex_coord_in;
    normal = norm;
    //frag_pos_light_space = light_space * vec4(frag_pos, 1.0);
    z = gl_Position.z / 100.0;
    for (int i=0; i<NUM_CASCADES; i++) {
        frag_pos_light_space[i] = light_spaces[i] * vec4(frag_pos, 1.0);
    }
}
