#version 300 es
precision highp float;
in vec3 pos;
uniform mat4 light_space;
uniform mat4 model;
uniform int cascade;
void main() {
    gl_Position = (light_space * model * vec4(pos, 1.0));
}
