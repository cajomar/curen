My OpenGL Cube Renderer
=======================

My second attempt, this time in C (the first was in Python).  

Dependencies
============
The cube renderer needs GLFW3.  
You can probably install it with a package manager (package `libglfw3-dev` for Debian and packages `glfw-x11` or `glfw-wayland` for Arch)  

Unfortunatly I haven't had success with using the package managers version, so if that doesn't work then you need to build GLFW yourself:
First clone it if you don't have it:
```console
$ git clone https://github.com/glfw/glfw.git
```
Then you can either install it:
```console
$ cd glfw
$ mkdir build && cd build
$ cmake .. -DBUILD_SHARED_LIBS=ON && make -j4 && sudo make install
```
Or uncomment these lines in Curen's CMakeLists.txt file by removing the `#` symbol:
```cmake
add_subdirectory("${CMAKE_SOURCE_DIR}/../glfw" glfw)
include_directories("${CMAKE_SOURCE_DIR}/../glfw/include")
```
and make sure that GLFW is in Curen's parent directory.

Building
========
```console
$ mkdir build && cd build
$ cmake ..
$ make -j
$ cd ..
```
Then execute the executable:  
```console
$ ./build/curen
```
