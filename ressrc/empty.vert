#version 300 es

precision highp float;

in vec3 pos;
in vec2 tex_coord_in;
in vec3 norm;

uniform mat4 player_space;
uniform mat4 model;

void main() {
    gl_Position = player_space * model * vec4(pos, 1.0);
}
