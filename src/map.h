#ifndef __CUREN_MAP__
#define __CUREN_MAP__

#include "chunk.h"
#include "player.h"

#define BIN_SIZE 32
#define CHUNK_TO_BIN(x) ((x) / BIN_SIZE)
#define BIN_TO_CHUNK(x) ((x) * BIN_SIZE)

typedef struct chunk_bin {
    Chunk* chunks;  // stretchy_buffer
    // A bitvector representing if there has been an attemt to build each chunk
    // sizeof(*loaded_chunk) MUST == BIN_SIZE
    uint32_t loaded_chunks[BIN_SIZE * BIN_SIZE];
    // Bin sizes
    int x, y, z;
} ChunkBin;

typedef struct map {
    ChunkBin* bins;  // stretchy_buffer
    Player player;
} Map;

int flatten_cube(int x, int y, int z, Map* m);
int flatten_chunk(int x, int y, int z, Map* m);
Map* map_create(void);
void map_destroy(Map* map);
Chunk* map_get_chunk_in(Map* map, int x, int y, int z);
Chunk* map_get_chunk_at(Map* map, int x, int y, int z);
Chunk* map_find_chunk_in(Map* map, int x, int y, int z);
Chunk* map_find_chunk_at(Map* map, int x, int y, int z);
const Chunk** map_get_chunks_to_render(Map* map);

#endif
