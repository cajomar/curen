#include "map.h"

#include <stdio.h>
#include <stdlib.h>

#include "renderer.h"
#include "stretchy_buffer.h"

Map* map_create() {
    Map* map = calloc(1, sizeof(Map));
    player_init(&map->player);

#define START_MAP_SIZE 3

    for (int z = 0; z < START_MAP_SIZE; z++) {
        for (int y = 0; y < START_MAP_SIZE; y++) {
            for (int x = 0; x < START_MAP_SIZE; x++) {
                map_get_chunk_at(map, x, y, z);
            }
        }
    }
    return map;
}

void map_destroy(Map* map) {
    sbforeachp(ChunkBin * b, map->bins) { sbfree(b->chunks); }
    sbfree(map->bins);
    free(map);
    map = 0;
}

/*
 * Return the the bin that a chunk at xyz (in chunk sizes) would be in
 */
static ChunkBin* map_get_bin_to_place_chunk(Map* map, int x, int y, int z) {
    int empty_bin = -1;
    sbforeachp(ChunkBin * b, map->bins) {
        if (sbcount(b->chunks) == 0) {
            empty_bin = stb__counter;
        }
        if (b->x == CHUNK_TO_BIN(x) && b->y == CHUNK_TO_BIN(y) &&
            b->z == CHUNK_TO_BIN(z)) {
            return b;
        }
    }

    ChunkBin* b;
    if (empty_bin > -1) {
        b = &map->bins[empty_bin];
    } else {
        b = sbadd(map->bins, 1);
        b->chunks = NULL;
    }
    memset(b->loaded_chunks, 0, BIN_SIZE * BIN_SIZE * BIN_SIZE / 8);
    b->x = CHUNK_TO_BIN(x);
    b->y = CHUNK_TO_BIN(y);
    b->z = CHUNK_TO_BIN(z);
    return b;
}

/*
 * Return the chunk that contains the cube coorinates xyz
 */
Chunk* map_find_chunk_in(Map* map, int x, int y, int z) {
    return map_find_chunk_at(map, CUBE_TO_CHUNK(x), CUBE_TO_CHUNK(y),
                             CUBE_TO_CHUNK(z));
}

/*
 * Return the chunk at xyz (in chunk coordinates)
 */
Chunk* map_find_chunk_at(Map* map, int x, int y, int z) {
    sbforeachp(ChunkBin * b, map->bins) {
        if (b->x == CHUNK_TO_BIN(x) && b->y == CHUNK_TO_BIN(y) &&
            b->z == CHUNK_TO_BIN(z)) {
            sbforeachp(Chunk * c, b->chunks) {
                if (x == c->x && y == c->y && z == c->z) {
                    return c;
                }
            }
            // The chunk should be in this bin, but it isn't
            return NULL;
        }
    }
    return NULL;
}

/*
 * Return the chunk that contains the cube coorinates xyz, creating it if it
 * does not exist
 */
Chunk* map_get_chunk_in(Map* map, int x, int y, int z) {
    return map_get_chunk_at(map, CUBE_TO_CHUNK(x), CUBE_TO_CHUNK(y),
                            CUBE_TO_CHUNK(z));
}

/*
 * xyz in global chunk sizes
 */
static bool chunk_has_beed_loaded(ChunkBin* b, int x, int y, int z) {
    int i = (z % BIN_SIZE) * BIN_SIZE + (y % BIN_SIZE);
    assert(i < BIN_SIZE * BIN_SIZE);
    return b->loaded_chunks[i] & (1 << (x % BIN_SIZE));
}

/*
 * xyz in global chunk sizes
 */
static void set_chunk_has_beed_loaded(ChunkBin* b, int x, int y, int z) {
    b->loaded_chunks[(z % BIN_SIZE) * BIN_SIZE + (y % BIN_SIZE)] |=
        (1 << (x % BIN_SIZE));
}

/*
 * Return the chunk at xyz (in chunk coordinates), creating it if it does not
 * exist
 */
Chunk* map_get_chunk_at(Map* map, int x, int y, int z) {
    if (x < 0 || y < 0 || z < 0) return NULL;
    Chunk* c = map_find_chunk_at(map, x, y, z);
    if (c == NULL) {
        ChunkBin* b = map_get_bin_to_place_chunk(map, x, y, z);
        assert(b);
        if (!chunk_has_beed_loaded(b, x, y, z)) {
            set_chunk_has_beed_loaded(b, x, y, z);
            c = sbadd(b->chunks, 1);
            chunk_init(c, x, y, z);
            if (chunk_build_terrain(c)) {
                // chunk_build_verts(c);
            } else {
                sbremovelast(b->chunks);
                chunk_deinit(c);
                c = NULL;
            }
        }
    }
    return c;
}

static bool in_view_direction(vec3 a) {
    return (!(fabs(a[0]) > 1 || fabs(a[1]) > 1 || fabs(a[2]) > 1));
}

// int (*map_get_chunks_to_render(Map* map))[2] {
// int (*render_chunks)[2] = 0;
const Chunk** map_get_chunks_to_render(Map* map) {
    // In bin sizes
    int left = (map->player.pos[0] - FAR_PLANE) / CHUNK_SIZE - 1;
    int right = (map->player.pos[0] + FAR_PLANE) / CHUNK_SIZE + 1;
    int bottom = (map->player.pos[1] - FAR_PLANE) / CHUNK_SIZE - 1;
    int top = (map->player.pos[1] + FAR_PLANE) / CHUNK_SIZE + 1;
    int back = (map->player.pos[2] - FAR_PLANE) / CHUNK_SIZE - 1;
    int front = (map->player.pos[2] + FAR_PLANE) / CHUNK_SIZE + 1;

    int num_chunks_built = 0;

    /* sbforeachp(ChunkBin * b, map->bins) {
        // Check if bin is in view
        if (left <= BIN_TO_CHUNK(b->x) && BIN_TO_CHUNK(b->x) <= right &&
            bottom <= BIN_TO_CHUNK(b->y) && BIN_TO_CHUNK(b->y) <= top &&
            back <= BIN_TO_CHUNK(b->z) && BIN_TO_CHUNK(b->z) <= front)
        {

        }
    } */

    for (int z = back; z < front; z++) {
        for (int y = bottom; y < top; y++) {
            for (int x = left; x < right; x++) {
                // Cube sizes
                vec3 center = {
                    x * CHUNK_SIZE + CHUNK_SIZE / 2,
                    y * CHUNK_SIZE + CHUNK_SIZE / 2,
                    z * CHUNK_SIZE + CHUNK_SIZE / 2,
                };
#if 1
                // Check to see if the chunk is behind us, except the chunk
                // we're in
                vec3 dist, angle;
                vec3_sub(dist, center, map->player.pos);
                vec3_norm(angle, dist);
                vec3_sub(angle, angle, map->player.front);
                if (vec3_mul_inner(dist, dist) > CHUNK_SIZE * CHUNK_SIZE &&
                    !in_view_direction(angle))
                    continue;
#endif

                if (num_chunks_built < MAX_BUILD_CHUNKS_PER_STEP) {
                    Chunk* chunk = map_get_chunk_at(map, x, y, z);
                    if (chunk && chunk->state == CHUNK_STATE_dirty) {
                        chunk_build_verts(chunk);
                        num_chunks_built++;
                    }
                }
            }
        }
    }

    const Chunk** render_chunks = 0;  // stretchy_buffer

    for (int z = back; z < front; z++) {
        for (int y = bottom; y < top; y++) {
            for (int x = left; x < right; x++) {
                // Cube sizes
                vec3 center = {
                    x * CHUNK_SIZE + CHUNK_SIZE / 2,
                    y * CHUNK_SIZE + CHUNK_SIZE / 2,
                    z * CHUNK_SIZE + CHUNK_SIZE / 2,
                };
#if 1
                // Check to see if the chunk is behind us, except the chunk
                // we're in
                vec3 dist, angle;
                vec3_sub(dist, center, map->player.pos);
                vec3_norm(angle, dist);
                vec3_sub(angle, angle, map->player.front);
                if (vec3_mul_inner(dist, dist) > CHUNK_SIZE * CHUNK_SIZE &&
                    !in_view_direction(angle))
                    continue;
#endif

                Chunk* chunk = map_find_chunk_at(map, x, y, z);
                if (chunk) {
                    if (chunk->state == CHUNK_STATE_built &&
                        chunk->num_vertices > 0) {
                        // WARNING WARNING WARNING!!! Don't use map_get_chunk_*
                        // or modify the chunks/bins in any way. I'm saving
                        // pointers into the chunks till the end of the funtion.
                        sbpush(render_chunks, chunk);
                    }
                }
            }
        }
    }
    return render_chunks;
}
