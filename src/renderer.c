#include "renderer.h"

#include <stdbool.h>
#include <stdio.h>

#include "chunk.h"
#include "shader.h"
#include "stb_image.h"
#include "stretchy_buffer.h"

#define DEPTH_MAP_RESOLUTION (1024 * 2)
#define NUM_CASCADES 4

#if 1
#define GLDEBUG(x)                                                         \
    x;                                                                     \
    {                                                                      \
        GLenum e;                                                          \
        if ((e = glGetError()) != GL_NO_ERROR) {                           \
            printf("glError 0x%x at %s line %d\n", e, __FILE__, __LINE__); \
            assert(false);                                                 \
        }                                                                  \
    }
#else
#define GLDEBUG(x) (x)
#endif

#define SHADOW 0

static void print_vec3(const char* s, vec3 a) {
    printf("%s: %f %f %f\n", s, a[0], a[1], a[2]);
}

// Globals
vec3 up = {0.f, 1.f, 0.f};
bool show_shadow_debug = false;

extern unsigned int viewport[2];

static GLuint land_texture;
static GLuint land_shader;

#if SHADOW
static GLuint depth_shader;
static GLuint debug_shadow_shader;
static GLuint quad_vao, quad_vbo;
static GLuint cascade_fbo;
static GLuint cascade_texture;
static const float cascade_ranges[NUM_CASCADES + 1] = {
    NEAR_PLANE / FAR_PLANE * 2, 0.09f, 0.13f, 0.3f, 0.8f,
};
#endif

static vec3 light_direction;

void renderer_init() {
#if SHADOW
    land_shader = shader_new("res/landshadow.vert", "res/landshadow.frag");
#else
    land_shader = shader_new("res/land.vert", "res/land.frag");
#endif

    GLDEBUG(glUseProgram(land_shader));
    shader_set_int(land_shader, "land_texture", 0);
    shader_set_int(land_shader, "cascade_texture", 1);
    light_direction[0] = -0.2f;
    light_direction[1] = -0.8f;
    light_direction[2] = -0.3f;
    vec3_norm(light_direction, light_direction);
    shader_set_vec3(land_shader, "light_direction", light_direction);

#if SHADOW
    GLDEBUG(glUniform1fv(glGetUniformLocation(land_shader, "cascade_ranges"),
                         NUM_CASCADES + 1, cascade_ranges));
#endif

    // Set up the land texture atlas
    GLDEBUG(glGenTextures(1, &land_texture));
    stbi_set_flip_vertically_on_load(true);
    {
        int width, height, num_channels;
        unsigned char* data =
            stbi_load("res/texture.png", &width, &height, &num_channels, 3);
        assert(num_channels == 3);

        GLDEBUG(glBindTexture(GL_TEXTURE_2D, land_texture));
        GLDEBUG(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                             GL_UNSIGNED_BYTE, data));
        GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        stbi_image_free(data);
    }
#if SHADOW
    GLDEBUG(glGenFramebuffers(1, &cascade_fbo));
    GLDEBUG(glGenTextures(1, &cascade_texture));
    GLDEBUG(glBindTexture(GL_TEXTURE_2D, cascade_texture));

#ifdef __EMSCRIPTEN__
    GLDEBUG(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16,
                         DEPTH_MAP_RESOLUTION, DEPTH_MAP_RESOLUTION, 0,
                         GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, NULL));
#else
    GLDEBUG(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                         DEPTH_MAP_RESOLUTION, DEPTH_MAP_RESOLUTION, 0,
                         GL_DEPTH_COMPONENT, GL_FLOAT, NULL));
#endif

    /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); */
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

#ifdef __EMSCRIPTEN__
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
#else
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER));
    GLDEBUG(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER));
    const vec4 white = {1.f, 1.f, 1.f, 1.f};
    GLDEBUG(glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, white));
#endif

    GLDEBUG(glBindFramebuffer(GL_FRAMEBUFFER, cascade_fbo));
    GLDEBUG(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                   GL_TEXTURE_2D, cascade_texture, 0));
#ifdef __EMSCRIPTEN__
    GLenum a = GL_NONE;
    GLDEBUG(glDrawBuffers(1, &a));
#else
    GLDEBUG(glDrawBuffer(GL_NONE));
#endif
    GLDEBUG(glReadBuffer(GL_NONE));
    GLDEBUG(glBindFramebuffer(GL_FRAMEBUFFER, 0));

    depth_shader = shader_new("res/depth.vert", "res/empty.frag");
    debug_shadow_shader =
        shader_new("res/texture_plane.vert", "res/debug_depth_map.frag");

    const float quad_verts[4 * 5] = {
        // Positions        tex coords
        -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f,  -1.0f, 0.0f, 1.0f, 0.0f,
    };

    GLDEBUG(glGenVertexArrays(1, &quad_vao));
    GLDEBUG(glGenBuffers(1, &quad_vbo));

    GLDEBUG(glBindVertexArray(quad_vao));
    GLDEBUG(glBindBuffer(GL_ARRAY_BUFFER, quad_vbo));

    GLDEBUG(glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 20, quad_verts,
                         GL_STATIC_DRAW));

    GLDEBUG(glEnableVertexAttribArray(0));
    GLDEBUG(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                                  (void*)0));

    GLDEBUG(glEnableVertexAttribArray(1));
    GLDEBUG(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                                  (void*)(3 * sizeof(float))));
#endif

    GLDEBUG(glClearColor(0.4f, 0.9f, 1.f, 1.f));

    GLDEBUG(glEnable(GL_CULL_FACE));
    GLDEBUG(glEnable(GL_DEPTH_TEST));
}

void renderer_shutdown() {
#if SHADOW
    GLDEBUG(glDeleteVertexArrays(1, &quad_vao));
    GLDEBUG(glDeleteBuffers(1, &quad_vbo));
    GLDEBUG(glDeleteFramebuffers(1, &cascade_fbo));
    GLDEBUG(glDeleteProgram(debug_shadow_shader));
    GLDEBUG(glDeleteProgram(depth_shader));
#endif
    GLDEBUG(glDeleteProgram(land_shader));
}

void renderer_draw(const Map* map, const Chunk* const* const render_chunks) {
    GLDEBUG(glClearColor(0.1f, 0.1f, 0.1f, 1.0f));

    const float y_fov = M_PI / 4.f;
    const float aspect = viewport[0] / (float)viewport[1];

    vec3 target;
    vec3_add(target, map->player.pos, map->player.front);
    mat4x4 view;
    mat4x4_look_at(view, map->player.pos, target, up);

#if SHADOW
    mat4x4 inverse_view;
    mat4x4_invert(inverse_view, view);

    mat4x4 light_view;
    vec3 origin = {0.f, 0.f, 0.f};
    mat4x4_look_at(light_view, origin, light_direction, up);
    mat4x4 player_to_light;
    mat4x4_mul(player_to_light, light_view, inverse_view);

    GLDEBUG(glBindFramebuffer(GL_FRAMEBUFFER, cascade_fbo));
    GLDEBUG(glClear(GL_DEPTH_BUFFER_BIT));
    // glCullFace(GL_FRONT);

    mat4x4 light_spaces[NUM_CASCADES];
    for (int c = 0; c < NUM_CASCADES; c++) {
        GLDEBUG(glViewport((c % 2) * DEPTH_MAP_RESOLUTION / 2,
                           (c >= 2) * DEPTH_MAP_RESOLUTION / 2,
                           DEPTH_MAP_RESOLUTION / 2, DEPTH_MAP_RESOLUTION / 2));

        const float near_x =
            tanf(y_fov / 2.f * aspect) * cascade_ranges[c] * FAR_PLANE;
        const float far_x =
            tanf(y_fov / 2.f * aspect) * cascade_ranges[c + 1] * FAR_PLANE;
        const float near_y = tanf(y_fov / 2.f) * cascade_ranges[c] * FAR_PLANE;
        const float far_y =
            tanf(y_fov / 2.f) * cascade_ranges[c + 1] * FAR_PLANE;

        vec4 player_view_corners[8] = {
            {near_x, near_y, -cascade_ranges[c] * FAR_PLANE, 1.f},
            {-near_x, near_y, -cascade_ranges[c] * FAR_PLANE, 1.f},
            {near_x, -near_y, -cascade_ranges[c] * FAR_PLANE, 1.f},
            {-near_x, -near_y, -cascade_ranges[c] * FAR_PLANE, 1.f},
            {far_x, far_y, -cascade_ranges[c + 1] * FAR_PLANE, 1.f},
            {-far_x, far_y, -cascade_ranges[c + 1] * FAR_PLANE, 1.f},
            {far_x, -far_y, -cascade_ranges[c + 1] * FAR_PLANE, 1.f},
            {-far_x, -far_y, -cascade_ranges[c + 1] * FAR_PLANE, 1.f},
        };
        vec4 max, min;
        // Initalize max and min to the first one
        mat4x4_mul_vec4(max, player_to_light, player_view_corners[0]);
        memcpy(min, max, sizeof(vec4));
        for (int i = 1; i < 8; ++i) {
            vec4 l;
            mat4x4_mul_vec4(l, player_to_light, player_view_corners[i]);
            vec4_max(max, max, l);
            vec4_min(min, min, l);
        }

        mat4x4 light_projection;

        /*
        vec4 player_pos_light_space;
        vec4 pp;
        memcpy(pp, player.pos, sizeof(vec3));
        pp[3] = 1.f;
        mat4x4_mul_vec4(player_pos_light_space, light_view, pp);
        min[2] = player_pos_light_space[2]-100.f;
        max[2] = player_pos_light_space[2]+100.f;
        */
        min[2] = -50.f;
        max[2] = 50.f;
        mat4x4_ortho(light_projection, min[0], max[0], min[1], max[1],
                     min[2] - 50.f, max[2] + 50.f);

        // printf("%f %f\n", min[2], max[2]);

        mat4x4_mul(light_spaces[c], light_projection, light_view);

        // Render depth map
        GLDEBUG(glUseProgram(depth_shader));
        shader_set_mat4x4(depth_shader, "light_space", light_spaces[c]);
        sbforeachv(const Chunk* c, render_chunks) { chunk_draw(c); }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // glCullFace(GL_BACK);
#endif

    // Render color
    GLDEBUG(glClearColor(0.4f, 0.9f, 1.f, 1.f));
    GLDEBUG(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    GLDEBUG(glViewport(0, 0, viewport[0], viewport[1]));

    GLDEBUG(glUseProgram(land_shader));

    GLDEBUG(glActiveTexture(GL_TEXTURE0));
    GLDEBUG(glBindTexture(GL_TEXTURE_2D, land_texture));

#if SHADOW
    GLDEBUG(glActiveTexture(GL_TEXTURE1));
    GLDEBUG(glBindTexture(GL_TEXTURE_2D, cascade_texture));
#endif

    mat4x4 projection;
    mat4x4_perspective(projection, y_fov, aspect, NEAR_PLANE, FAR_PLANE);
    // mat4x4_ortho(projection, -10, 10, -10, 10, NEAR_PLANE, FAR_PLANE);
    mat4x4 player_space;

    mat4x4_mul(player_space, projection, view);

    GLDEBUG(glUseProgram(land_shader));
    shader_set_mat4x4(land_shader, "player_space", player_space);

#if SHADOW
    GLDEBUG(
        glUniformMatrix4fv(glGetUniformLocation(land_shader, "light_spaces"),
                           NUM_CASCADES, GL_FALSE, (float*)light_spaces));
#endif

    shader_set_vec3(land_shader, "view_pos", map->player.pos);

    /* sbforeachp(const ChunkBin* b, map->bins) {
        sbforeachp(const Chunk* c, b->chunks) { chunk_draw(c); }
    } */

    sbforeachv(const Chunk* c, render_chunks) { chunk_draw(c); }
    /* for (int i = 0; i < map->width * map->height * map->depth; i++) {
        Chunk* chunk = &map->chunks[i];
        if (chunk->state == CHUNK_STATE_ungenerated) {
            chunk_build_terrain(chunk);
            chunk_build_verts(chunk);
        } else if (chunk->state == CHUNK_STATE_dirty) {
            chunk_build_verts(chunk);
        }
        chunk_draw(chunk);
    } */

#if SHADOW
    if (show_shadow_debug) {
        // Render the depth map
        GLDEBUG(glClear(GL_DEPTH_BUFFER_BIT));
        GLDEBUG(glViewport(0, 0, DEPTH_MAP_RESOLUTION, DEPTH_MAP_RESOLUTION));

        GLDEBUG(glUseProgram(debug_shadow_shader));

        GLDEBUG(glActiveTexture(GL_TEXTURE0));
        GLDEBUG(glBindTexture(GL_TEXTURE_2D, cascade_texture));
        GLDEBUG(glBindVertexArray(quad_vao));
        GLDEBUG(glDrawArrays(GL_TRIANGLE_STRIP, 0, 4));
    }
#endif
}
