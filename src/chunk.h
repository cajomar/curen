#ifndef __CUREN_CHUNK__
#define __CUREN_CHUNK__

#define CHUNK_SIZE 16

#define CHUNK_TO_CUBE(x) ((x) * CHUNK_SIZE)
#define CUBE_TO_CHUNK(x) ((x) / CHUNK_SIZE)

//#define DEBUG

#ifdef DEBUG
#define INFORM(...) printf(__VA_ARGS__);
#else
#define INFORM(...) ;
#endif

#include <stdbool.h>
#include <stdint.h>

#include "opengl.h"

enum MapCubeType {
    MAP_CUBE_TYPE_empty = 0,
    MAP_CUBE_TYPE_grass,
    MAP_CUBE_TYPE_dirt,
    MAP_CUBE_TYPE_stone,
    MAP_CUBE_TYPE_end
};

enum ChunkState {
    // Chunk has no terrain
    CHUNK_STATE_ungenerated = 0,
    // Chunk's terrain is different from vertices
    CHUNK_STATE_dirty,
    // Chunk's terrain and vertices are correct
    CHUNK_STATE_built
};

typedef struct vertex {
    float x;
    float y;
    float z;
    float tcx;
    float tcy;
    float normx;
    float normy;
    float normz;
} Vertex;

typedef struct chunk {
    // Chunk sizes
    int x, y, z;
    GLuint vao, vbo;
    unsigned int num_vertices;
    uint8_t state;
    uint8_t cubes[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
} Chunk;

int cube_flatten_in_chunk(int x, int y, int z);
void chunk_init(Chunk* chunk, int x, int y, int z);
void chunk_deinit(Chunk* chunk);
bool chunk_build_terrain(Chunk* chunk);
void chunk_build_verts(Chunk* chunk);
void chunk_draw(const Chunk* chunk);

#endif
