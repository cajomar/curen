#include "chunk.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "map.h"
#include "mat.h"
#include "perlin.h"
#include "stretchy_buffer.h"

extern Map* map;

static const int cube_vertices[8][3] = {
    // Position  tex coord
    //  x  y  z
    {0, 0, 0},  // 0
    {0, 0, 1},  // 1
    {0, 1, 0},  // 2
    {0, 1, 1},  // 3
    {1, 0, 0},  // 4
    {1, 0, 1},  // 5
    {1, 1, 0},  // 6
    {1, 1, 1},  // 7
};

static const float tex_coords[12] = {
    0.f, 0.f, .5f, 0.f, 0.f, .5f, .5f, .5f, 0.f, .5f, .5f, 0.f,
};

static const int sides[6][6] = {
    // Left side
    {0, 1, 2, 3, 2, 1},
    // Right side
    {5, 4, 7, 6, 7, 4},
    // Bottom side
    {0, 4, 1, 5, 1, 4},
    // Top side
    {3, 7, 2, 6, 2, 7},
    // Back side
    {4, 0, 6, 2, 6, 0},
    // Front side
    {1, 5, 3, 7, 3, 5},
};

static const int neighbors[6][3] = {
    {-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}, {0, 0, -1}, {0, 0, 1},
};

int cube_flatten_in_chunk(int x, int y, int z) {
    return (y + (z * CHUNK_SIZE)) * CHUNK_SIZE + x;
}

static bool cube_outside_chunk(int x, int y, int z) {
    return (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 ||
            z >= CHUNK_SIZE);
}

void chunk_init(Chunk* chunk, int x, int y, int z) {
    chunk->x = x;
    chunk->y = y;
    chunk->z = z;
    glGenBuffers(1, &chunk->vbo);
    glGenVertexArrays(1, &chunk->vao);
    chunk->num_vertices = 0;
    chunk->state = CHUNK_STATE_ungenerated;
    memset(chunk->cubes, 0,
           sizeof(*chunk->cubes) * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
}

void chunk_deinit(Chunk* chunk) {
    glDeleteVertexArrays(1, &chunk->vao);
    glDeleteBuffers(1, &chunk->vbo);
}

// Cube size
#define TERRAIN_HEIGHT_REPEAT_SIZE 1024

static int get_height(int x, int z) {
    z %= TERRAIN_HEIGHT_REPEAT_SIZE * 2;
    if (z > TERRAIN_HEIGHT_REPEAT_SIZE) z = TERRAIN_HEIGHT_REPEAT_SIZE * 2 - z;
    float fz = z / (float)TERRAIN_HEIGHT_REPEAT_SIZE;

    x %= TERRAIN_HEIGHT_REPEAT_SIZE * 2;
    if (x > TERRAIN_HEIGHT_REPEAT_SIZE) x = TERRAIN_HEIGHT_REPEAT_SIZE * 2 - x;
    float fx = x / (float)TERRAIN_HEIGHT_REPEAT_SIZE;

    float h = perlin2d(fx, fz, 12.f, 7);
    h = powf(h, 3.2f);
    return h * 92;
}

/*
 * Fill in the terrain for a chunk, return true if this chunk is visible
 */
bool chunk_build_terrain(Chunk* chunk) {
    // Generate chunk's terrain
    memset(chunk->cubes, 0,
           sizeof(*chunk->cubes) * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);

    bool chunk_has_terrain = false;
    bool chunk_is_surface = false;

    for (int z = 0; z < CHUNK_SIZE; z++) {
        for (int x = 0; x < CHUNK_SIZE; x++) {
            int pos_height = get_height(CHUNK_TO_CUBE(chunk->x) + x,
                                        CHUNK_TO_CUBE(chunk->z) + z);

            if (pos_height >= CHUNK_TO_CUBE(chunk->y)) {
                chunk_has_terrain = true;

                if (pos_height < CHUNK_TO_CUBE(chunk->y + 1)) {
                    chunk_is_surface = true;
                }

                for (int y = 0; y < CHUNK_SIZE &&
                                y + CHUNK_TO_CUBE(chunk->y) <= pos_height;
                     y++) {
                    // chunk->cubes[flatten(x, y, z)] = MAP_CUBE_TYPE_dirt;
                    /* int yy = (y + CHUNK_TO_CUBE(chunk->y)) %
                             (TERRAIN_HEIGHT_REPEAT_SIZE * 2);
                    if (yy > TERRAIN_HEIGHT_REPEAT_SIZE)
                        yy = TERRAIN_HEIGHT_REPEAT_SIZE * 2 - yy; */

                    // if (perlin3d(xx, yy, zz, 0.05f, 3) < .8) {
                    chunk->cubes[cube_flatten_in_chunk(x, y, z)] =
                        MAP_CUBE_TYPE_dirt;
                    /* } else {
                        chunk->cubes[cube_flatten_in_chunk(x, y, z)] =
                            MAP_CUBE_TYPE_stone;
                    } */
                    if (y + CHUNK_TO_CUBE(chunk->y) == pos_height) {
                        chunk->cubes[cube_flatten_in_chunk(x, y, z)] =
                            MAP_CUBE_TYPE_grass;
                    }
                }
            }
        }
    }

    chunk->state = CHUNK_STATE_dirty;
    return chunk_has_terrain && chunk_is_surface;
}

static void add_side(Vertex** vertices, int cube_x, int cube_y, int cube_z,
                     enum MapCubeType cube_type, int side) {
    Vertex* verts = sbadd(*vertices, 6);
    for (int v = 0; v < 6; v++) {
        Vertex vert;
        vert.x = cube_x + cube_vertices[sides[side][v]][0];
        vert.y = cube_y + cube_vertices[sides[side][v]][1];
        vert.z = cube_z + cube_vertices[sides[side][v]][2];
        /* vert.tcx = 0.5f * ((cube_type - 1) % 2);
        vert.tcy = 0.5f * ((cube_type - 1) >= 2); */
        // texture coorinates
        vert.tcx = tex_coords[v * 2];
        switch (cube_type) {
            case MAP_CUBE_TYPE_grass:
                if (side == 2) {
                    vert.tcx = tex_coords[v * 2] + 0.5f;
                    vert.tcy = tex_coords[v * 2 + 1] + 0.5f;
                } else if (side == 3) {
                    vert.tcx = tex_coords[v * 2];
                    vert.tcy = tex_coords[v * 2 + 1] + 0.5f;
                } else {
                    vert.tcx = tex_coords[v * 2];
                    vert.tcy = tex_coords[v * 2 + 1];
                }
                break;
            case MAP_CUBE_TYPE_dirt:
                vert.tcx = tex_coords[v * 2] + 0.5f;
                vert.tcy = tex_coords[v * 2 + 1] + 0.5f;
                break;
            case MAP_CUBE_TYPE_stone:
                vert.tcx = tex_coords[v * 2] + 0.5f;
                vert.tcy = tex_coords[v * 2 + 1];
                break;
        }
        vert.normx = (float)sides[side][0];
        vert.normy = (float)sides[side][1];
        vert.normz = (float)sides[side][2];
        memcpy(&verts[v], &vert, sizeof(Vertex));
    }
}

void chunk_build_verts(Chunk* chunk) {
    // TODO Make an EBO?
    Vertex* vertices = 0;  // stretchy_buffer

    for (int z = 0; z < CHUNK_SIZE; z++) {
        for (int y = 0; y < CHUNK_SIZE; y++) {
            for (int x = 0; x < CHUNK_SIZE; x++) {
                enum MapCubeType cube_type =
                    chunk->cubes[cube_flatten_in_chunk(x, y, z)];
                if (cube_type == MAP_CUBE_TYPE_empty) {
                    continue;
                }
                for (int s = 0; s < 6; s++) {
                    // Local neighbor cube pos
                    int lncx = x + neighbors[s][0];
                    int lncy = y + neighbors[s][1];
                    int lncz = z + neighbors[s][2];
                    // TODO Don't build sides bordering chunks
                    if (cube_outside_chunk(lncx, lncy, lncz)) {
                        if (!neighbors[s][1]) {
                            // Absolute neighbor cube pos
                            int ancx = lncx + CHUNK_TO_CUBE(chunk->x);
                            int ancz = lncz + CHUNK_TO_CUBE(chunk->z);
                            int nh = get_height(ancx, ancz);
                            if (nh < y + CHUNK_TO_CUBE(chunk->y)) {
                                add_side(&vertices, x, y, z, cube_type, s);
                            }
                        } else {
                            int h = get_height(x + CHUNK_TO_CUBE(chunk->x),
                                               z + CHUNK_TO_CUBE(chunk->z));
                            if (h == y + CHUNK_TO_CUBE(chunk->y)) {
                                add_side(&vertices, x, y, z, cube_type, s);
                            }
                        }
                    } else if (chunk->cubes[cube_flatten_in_chunk(
                                   lncx, lncy, lncz)] == MAP_CUBE_TYPE_empty) {
                        add_side(&vertices, x, y, z, cube_type, s);
                    }
                }
            }
        }
    }

    chunk->num_vertices = sbcount(vertices);

    glBindBuffer(GL_ARRAY_BUFFER, chunk->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * chunk->num_vertices,
                 vertices, GL_STATIC_DRAW);

    glBindVertexArray(chunk->vao);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)(5 * sizeof(float)));
    glEnableVertexAttribArray(2);

    chunk->state = CHUNK_STATE_built;
    sbfree(vertices);
}

void chunk_draw(const Chunk* chunk) {
    if (chunk->state != CHUNK_STATE_built) return;

    glBindVertexArray(chunk->vao);

    mat4x4 model;
    mat4x4_identity(model);
    mat4x4_translate_in_place(model, CHUNK_TO_CUBE(chunk->x),
                              CHUNK_TO_CUBE(chunk->y), CHUNK_TO_CUBE(chunk->z));
    GLuint shader;
    glGetIntegerv(GL_CURRENT_PROGRAM, &shader);
    glUniformMatrix4fv(glGetUniformLocation(shader, "model"), 1, GL_FALSE,
                       &model[0][0]);

    glDrawArrays(GL_TRIANGLES, 0, chunk->num_vertices);
}
