#include "player.h"

#include <stdio.h>

extern GLFWwindow* window;
extern int viewport[2];
extern vec3 up;

void player_init(Player* player) {
    player->pos[0] = 10; 
    player->pos[1] = 10; 
    player->pos[2] = 10; 
    player->front[0] = 0;
    player->front[1] = 0;
    player->front[2] = 0;
    player->yaw = M_PI/4.f;
    player->pitch = 0.f;
}

void player_update(Player* player, float dt) {
    const float speed = 10.f;
    vec3 diff;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        vec3_scale(diff, player->front, speed * dt);
        vec3_add(player->pos, player->pos, diff);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        vec3_scale(diff, player->front, speed * dt);
        vec3_sub(player->pos, player->pos, diff);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        vec3_mul_cross(diff, player->front, up);
        vec3_norm(diff, diff);
        vec3_scale(diff, diff, speed * dt);
        vec3_sub(player->pos, player->pos, diff);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        vec3_mul_cross(diff, player->front, up);
        vec3_norm(diff, diff);
        vec3_scale(diff, diff, speed * dt);
        vec3_add(player->pos, player->pos, diff);
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        player->pos[1] += speed * dt;
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        player->pos[1] -= speed * dt;
}

void player_on_mouse_motion(Player* player, int x, int y) {
    float dx = x;
    float dy = y;

    float sensitivity = 0.001;
    dx *= sensitivity;
    dy *= sensitivity;

    player->yaw += dx;
    player->pitch += dy;

    if(player->pitch > M_PI/2.f-0.0001)
        player->pitch = M_PI/2.f-0.0001;
    if(player->pitch < -M_PI/2.f+0.0001) 
        player->pitch = -M_PI/2.f+0.0001;

    vec3 direction;
    direction[0] = cos(player->yaw) * cos(player->pitch);
    direction[1] = sin(player->pitch);
    direction[2] = sin(player->yaw) * cos(player->pitch);
    vec3_norm(player->front, direction);
}
