#ifndef __CUREN_GL_H__
#define __CUREN_GL_H__

#ifdef GLES3
#   include <GLES3/gl3.h>
#elif defined(GLES)
#   include <GLES2/gl2.h>
#else
// #   include "glad/glad.h"
#   include "gl_core_3_3.h"
#endif

#endif
