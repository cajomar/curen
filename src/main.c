#ifdef GLES3
#define GLFW_INCLUDE_ES3
#elif defined(GLES)
#define GLFW_INCLUDE_ES2
#else
#include "opengl.h"
#endif

#include <GLFW/glfw3.h>

#include <stdbool.h>
#include <stdio.h>

#include "map.h"
#include "player.h"
#include "renderer.h"
#include "stretchy_buffer.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>

EM_JS(int, get_canvas_width, (void), { return canvas.width; })
EM_JS(int, get_canvas_height, (void), { return canvas.height; })
#endif /* __EMSCRIPTEN__ */

// Globals
GLFWwindow* window;
unsigned int viewport[2] = {1024 + 512, 756 + 128};

extern bool show_shadow_debug;

static bool mouse_captured = false;
static int cursor_x, cursor_y;
static Map* map = NULL;

static void window_resize_callback(GLFWwindow* window, int w, int h) {
    viewport[0] = w;
    viewport[1] = h;
    glViewport(0, 0, w, h);
    printf("Window resized to %d %d\n", w, h);
}

static void mouse_motion_callback(GLFWwindow* window, double x, double y) {
    int lx = cursor_x;
    int ly = cursor_y;
    cursor_x = x;
    cursor_y = y;
    if (mouse_captured) player_on_mouse_motion(&map->player, x - lx, y - ly);
}

static void mouse_button_callback(GLFWwindow* window, int button, int action,
                                  int mods) {
    printf("Now I've got you!\n");
    if (action == GLFW_PRESS && !mouse_captured) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        mouse_captured = true;
    }
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action,
                         int mods) {
    if (action == GLFW_RELEASE) {
        if (key == GLFW_KEY_ESCAPE) {
            if (mouse_captured) {
                mouse_captured = false;
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            } else {
                glfwSetWindowShouldClose(window, true);
            }
        } else if (key == GLFW_KEY_TAB) {
            show_shadow_debug = !show_shadow_debug;
        }
    }
}

void error_callback(int code, const char* description) {
    // fprintf(stderr, "GLFW Error %d: %s\n", code, description);
    printf("GLFW Error %d: %s\n", code, description);
}

void loop() {
    static double lengths = 0;
    static int frames = 0;

    static float frame_start = 0;
    float now = glfwGetTime();
    float dt = now - frame_start;
    frame_start = now;

    lengths += dt;
    frames++;

#ifndef __EMSCRIPTEN__
    if (frames > 20) {
        double spf = lengths / 20.f;
        double fps = 1.f / spf;
        printf("%f\n", fps);
        lengths = 0;
        frames = 0;
    }
#endif

    glfwPollEvents();
    player_update(&map->player, dt);

    const Chunk** render_chunks = map_get_chunks_to_render(map);
    renderer_draw(map, render_chunks);
    sbfree(render_chunks);

    glfwSwapBuffers(window);
}

int main(int argc, char** argv) {
    glfwSetErrorCallback(error_callback);
    if (glfwInit() != GLFW_TRUE) {
        fputs("Failed to initalize glfw\n", stderr);
        return -1;
    }

#ifdef GLES3
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
#elif defined(GLES)
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
#else
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __EMSCRIPTEN__
    // printf("Somthing's going wrong with the shadow mapping on Chromium-based browsers making it appear that everything's a shadow. Use Firefox!\n");
    /* viewport[0] = get_canvas_width();
    viewport[1] = get_canvas_height(); */
    /* viewport[0] = 800;
    viewport[1] = 600; */
#else
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    viewport[0] = mode->width;
    viewport[1] = mode->height;
#endif

    window = glfwCreateWindow(viewport[0], viewport[1], "Caleb's Cube Renderer",
                              NULL, NULL);
    if (!window) {
        fprintf(stderr, "Failed in create glfw window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_motion_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetFramebufferSizeCallback(window, window_resize_callback);

#ifndef GLES
#ifndef GLES3
    if (!ogl_LoadFunctions()) {
        printf("Failed to load OpenGL functions\n");
        return -1;
    }
#endif
#endif
    printf("Using %s\n", glGetString(GL_VERSION));
    printf("Using %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    printf("Viewport: %d %d\n", viewport[0], viewport[1]);

    glViewport(0, 0, viewport[0], viewport[1]);

    map = map_create();
    renderer_init();

    // Draw before mouse input
    player_on_mouse_motion(&map->player, 0, 0);

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(loop, 0, 1);
#else
    while (!glfwWindowShouldClose(window)) {
        loop();
    }
#endif

    map_destroy(map);
    renderer_shutdown();
    glfwTerminate();
    return 0;
}
