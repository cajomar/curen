#ifndef __CUREN_PLAYER__
#define __CUREN_PLAYER__

#include "opengl.h"
#include <GLFW/glfw3.h>
#include "mat.h"

typedef struct player {
    vec3 pos;
    vec3 front;
    float yaw;
    float pitch;
} Player;

void player_init(Player* player);
void player_update(Player* player, float dt);
void player_on_mouse_motion(Player* player, int x, int y);

#endif
