#ifndef __CUREN_SHADER__
#define __CUREN_SHADER__

#include "opengl.h"
#include "mat.h"


GLuint shader_new(const char* vertex_shader_path, const char* fragment_shader_path);


void shader_set_mat4x4(GLuint shader, const char* field, const mat4x4 value);
void shader_set_vec3(GLuint shader, const char* field, const vec3 value);
void shader_set_float(GLuint shader, const char* field, float value);
void shader_set_int(GLuint shader, const char* field, int value);

#endif
