#include "shader.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 1
#define GLDEBUG(x)                                                         \
    x;                                                                     \
    {                                                                      \
        GLenum e;                                                          \
        if ((e = glGetError()) != GL_NO_ERROR) {                           \
            printf("glError 0x%x at %s line %d\n", e, __FILE__, __LINE__); \
            assert(false);                                                 \
        }                                                                  \
    }
#else
#define GLDEBUG(x) (x)
#endif

void read_file(char* out, int max_out, const char* fn) {
    FILE* fp = fopen(fn, "r");
    if (fp == NULL) {
        printf("Error: could not find file %s\n", fn);
        exit(-1);
    }
    fseek(fp, 0, SEEK_END);
    int count = ftell(fp);
    rewind(fp);
    if (count > max_out) printf("Allocate more space for your shader!\n");
    fread(out, count, 1, fp);
    fclose(fp);
}

GLuint shader_new(const char* vertex_shader_path,
                  const char* fragment_shader_path) {
    int success;
    char info_log[512];

    unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    GLDEBUG();

#define SHADER_SOURCE_SIZE 4096
    char shader_source[SHADER_SOURCE_SIZE];
    memset(shader_source, 0, SHADER_SOURCE_SIZE);
    read_file(shader_source, SHADER_SOURCE_SIZE, vertex_shader_path);

    const char* a = shader_source;

    GLDEBUG(glShaderSource(vertex_shader, 1, &a, NULL));
    GLDEBUG(glCompileShader(vertex_shader));
    // Check for compile errors
    GLDEBUG(glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success));
    if (!success) {
        GLDEBUG(glGetShaderInfoLog(vertex_shader, 512, NULL, info_log));
        printf("Failed to compile %s: %s\n", vertex_shader_path, info_log);
    }

    unsigned int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    memset(shader_source, 0, SHADER_SOURCE_SIZE);
    read_file(shader_source, SHADER_SOURCE_SIZE, fragment_shader_path);
    GLDEBUG(glShaderSource(fragment_shader, 1, &a, NULL));
    GLDEBUG(glCompileShader(fragment_shader));

    GLDEBUG(glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success));
    if (!success) {
        GLDEBUG(glGetShaderInfoLog(fragment_shader, 512, NULL, info_log));
        printf("Failed to compile %s: %s\n", fragment_shader_path, info_log);
    }

    GLuint id = glCreateProgram();
    GLDEBUG();
    GLDEBUG(glAttachShader(id, vertex_shader));
    GLDEBUG(glAttachShader(id, fragment_shader));
    GLDEBUG(glLinkProgram(id));

    GLDEBUG(glGetProgramiv(id, GL_LINK_STATUS, &success));
    if (!success) {
        GLDEBUG(glGetProgramInfoLog(id, 512, NULL, info_log));
        printf("Failed to link shader program: %s\n", info_log);
    }

    GLDEBUG(glDeleteShader(vertex_shader));
    GLDEBUG(glDeleteShader(fragment_shader));

    return id;
}

void shader_set_mat4x4(GLuint shader, const char* field, const mat4x4 value) {
    // puts("Setting!");
    GLDEBUG(glUseProgram(shader));
    GLDEBUG(glUniformMatrix4fv(glGetUniformLocation(shader, field), 1, GL_FALSE,
                               &value[0][0]));
}

void shader_set_vec3(GLuint shader, const char* field, const vec3 value) {
    // puts("Setting!");
    GLDEBUG(glUseProgram(shader));
    GLDEBUG(glUniform3fv(glGetUniformLocation(shader, field), 1, value));
}

void shader_set_float(GLuint shader, const char* field, float value) {
    // puts("Setting!");
    GLDEBUG(glUseProgram(shader));
    GLDEBUG(glUniform1f(glGetUniformLocation(shader, field), value));
}

void shader_set_int(GLuint shader, const char* field, int value) {
    // puts("Setting!");
    GLDEBUG(glUseProgram(shader));
    GLDEBUG(glUniform1i(glGetUniformLocation(shader, field), value));
}
