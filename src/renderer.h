#ifndef __CUREN_RENDERER__
#define __CUREN_RENDERER__

#include "map.h"

#define NEAR_PLANE 0.1f
#define FAR_PLANE 100.f
#define MAX_BUILD_CHUNKS_PER_STEP 5

void renderer_init(void);
void renderer_shutdown(void);
void renderer_draw(const Map* map, const Chunk* const* const render_chunks);

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

#endif
