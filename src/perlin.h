#ifndef __PERLIN_NOISE__
#define __PERLIN_NOISE__

float perlin2d(float x, float y, float freq, int depth);
float perlin3d(float x, float y, float z, float freq, int depth);

#endif
